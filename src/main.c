/*
 * Copyright (c) 2016 Open-RnD Sp. z o.o.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <gpio.h>
#include <misc/util.h>
#include <misc/printk.h>

#define LED0_GPIO_CONTROLLER	GPIO_LEDS_LED_0_GPIO_CONTROLLER
#define LED_PORT LED0_GPIO_CONTROLLER
#define LED GPIO_LEDS_LED_0_GPIO_PIN

/* change this to use another GPIO port */
#define SW0_GPIO_CONTROLLER GPIO_KEYS_BUTTON_0_GPIO_CONTROLLER
#define PORT	SW0_GPIO_CONTROLLER

/* change this to use another GPIO pin */
#define PIN     GPIO_KEYS_BUTTON_0_GPIO_PIN

/* change to use another GPIO pin interrupt config */
/*
 * If SW0_GPIO_FLAGS not defined used default EDGE value.
 * Change this to use a different interrupt trigger
 */
#define EDGE    (GPIO_INT_EDGE | GPIO_INT_ACTIVE_HIGH)

/* change this to enable pull-up/pull-down */
#define SW0_GPIO_FLAGS 0
#define PULL_UP GPIO_PUD_PULL_UP

/* Sleep time */
#define SLEEP_TIME	1000

extern int led_alert_handler(struct k_alert *alert)
{
	struct device *ledb;

	ledb = device_get_binding(LED_PORT);

	gpio_pin_configure(ledb, LED, GPIO_DIR_OUT);
	gpio_pin_write(ledb, LED, 1);
	k_sleep(SLEEP_TIME);
	gpio_pin_write(ledb, LED, 0);
	k_sleep(SLEEP_TIME);
	return 0;
}

K_ALERT_DEFINE(led_alert, led_alert_handler, 10);

void button_pressed(struct device *gpiob, struct gpio_callback *cb,
		    u32_t pins)
{
	k_alert_send(&led_alert);
}

static struct gpio_callback gpio_cb;

void main(void)
{
	struct device *gpiob;

	printk("Press the user defined button on the board\n");
	gpiob = device_get_binding(PORT);
	if (!gpiob) {
		printk("error\n");
		return;
	}

	gpio_pin_configure(gpiob, PIN,
			   GPIO_DIR_IN | GPIO_INT |  PULL_UP | EDGE);

	gpio_init_callback(&gpio_cb, button_pressed, BIT(PIN));

	gpio_add_callback(gpiob, &gpio_cb);
	gpio_pin_enable_callback(gpiob, PIN);
	
	while (1) {
		//u32_t val = 0;

		//gpio_pin_read(gpiob, PIN, &val);
		//k_sleep(SLEEP_TIME);
	}
}
